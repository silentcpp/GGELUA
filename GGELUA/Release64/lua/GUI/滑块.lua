--[[
    @Author       : baidwwy
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-03-09 04:05:06
--]]

local _ENV = require("GGE界面")
local _计算位置  = function (self,_x,_y)
    local x,y,pos = self._rect:取坐标()
    
    if self.宽度>self.高度 then--横向
        local w = self._rect.宽度-self._btn2.宽度
        pos = math.floor((_x-x)/w*self.最大值)
    else
        local h = self._rect.高度-self._btn2.高度
        pos = math.floor((_y-y)/h*self.最大值)
    end

    if pos~=self.位置 then
        self:置位置(pos)
        return true
    end
end

GUI滑块 = class("GUI滑块",GUI控件)
GUI滑块._type  = 14
function GUI滑块:初始化()
    
    self.位置   = 0
    self.最小值 = 0
    self.最大值 = 100
end

function GUI滑块:创建滑块按钮(name,...)
    self._rect = self:创建控件('_rect',...)--按钮的区域
    self._btn2 = self._rect:创建按钮("_btn2")
    self[name] = self._btn2
    return self._btn2
end

function GUI滑块:创建减少按钮(name,...)--上边或左边
    self._btn1 = self:创建按钮("_btn1",...)
    self[name] = self._btn1
    return self._btn1
end

function GUI滑块:创建增加按钮(name,...)--下边或右边
    self._btn3 = self:创建按钮("_btn3",...)
    self[name] = self._btn3
    return self._btn3
end

function GUI滑块:置位置(v)
    if v<self.最小值 then
        v = self.最小值
    end
    if v>self.最大值 then
        v = self.最大值
    end

    self.位置 = v

    if self.宽度>self.高度 then--横向
        local w = self._rect.宽度-self._btn2.宽度
        self._btn2:置坐标(math.floor(v/self.最大值*w),0)
    else
        local h = self._rect.高度-self._btn2.高度
        self._btn2:置坐标(0,math.floor(v/self.最大值*h))
    end

    return self
end

function GUI滑块:_消息事件(msg)
    GUI控件._消息事件(self,msg)
    if self._btn1 and msg.按钮弹起==self._btn1 then
        self:置位置(self.位置-10)
    elseif self._btn3 and msg.按钮弹起==self._btn3 then
        self:置位置(self.位置+10)
    end

    for _,v in ipairs(msg.鼠标) do
        if v.type==SDL.鼠标_按下 then
            if self._rect:检查点(v.x,v.y) and v.button==SDL.BUTTON_LEFT then--点击区域
                v.type = nil
                if _计算位置(self,v.x,v.y) then
                    local x,y = self._btn2:取坐标()
                    self:发送消息("滚动事件",x,y,self.位置,msg)
                end
            end
        elseif v.type==SDL.鼠标_移动 and v.state==SDL.BUTTON_LMASK and self._btn2:取状态()=='按下' then--拖动
            if _计算位置(self,v.x,v.y) then
                local x,y = self._btn2:取坐标()
                self:发送消息("滚动事件",x,y,self.位置,msg)
            end
        end
    end
    
end
return GUI滑块