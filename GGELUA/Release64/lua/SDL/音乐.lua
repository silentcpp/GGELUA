--[[
    @Author       : GGELUA
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-04-27 13:39:07
--]]
local _ENV = require("SDL")
MIX_Init()

local SDL音乐 = class("SDL音乐")

function SDL音乐:初始化(t)
    local tp = ggetype(t)
    if tp == 'string' then
        self._mus = MIX.LoadMUS(t)
    elseif tp == 'SDL读写' then
        self._rw = t
        self._mus = MIX.LoadMUS_RW(t:取对象())
    end
    
    if self._mus then
        _mixs[self] = self._mus
    else
        error(GetError(),3)
    end
end

function SDL音乐:播放(loop)
    if type(loop)=='number' then
        self._mus:PlayMusic(loop)
    else
        self._mus:PlayMusic(loop and -1 or 0)
    end
    return self
end

function SDL音乐:停止()
    MIX.HaltMusic()
end

function SDL音乐:暂停()
    MIX.PauseMusic()
    return self
end

function SDL音乐:恢复()
    MIX.ResumeMusic()
    return self
end

function SDL音乐:重置()
    MIX.RewindMusic()
    return self
end

function SDL音乐:是否暂停()
    return MIX.PausedMusic()
end

function SDL音乐:是否播放()
    return MIX.PlayingMusic()
end

function SDL音乐:置音量(v)
    MIX.VolumeMusic(v)
    return self
end

function SDL音乐:取音量()
    return MIX.VolumeMusic(-1)
end

function SDL音乐:置位置(v)
    MIX.SetMusicPosition(v)
    return self
end

return SDL音乐