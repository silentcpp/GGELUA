#include "lua-nng.h"

void handle_callback(nng_aio *aio){
	
}

//handler_alloc(string path,function callback) :: http_handler 
int lnng_http_handler_alloc(lua_State *L){

}

static const struct luaL_Reg nng_http_handler_m[] = {
	{NULL, NULL}
};

static const struct luaL_Reg nng_http_f[] = {
	{"handler_alloc",lnng_http_handler_alloc},
	{NULL, NULL}
};

__declspec(dllexport) int luaopen_gnng_http(lua_State *L){
	luaL_newmetatable(L,"nng.http.handler");
	luaL_newlib(L,nng_http_handler_m);
	lua_setfield(L,-2,"__index");
	lua_pop(L,1);
	
	luaL_newlib(L,nng_http_f);
	return 1;
}
