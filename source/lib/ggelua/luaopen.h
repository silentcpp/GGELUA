#pragma once
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

unsigned int g_tohash(const char* path);
int LUA_LoadScriptPack(lua_State* L);
int LUA_GetScriptData(lua_State* L);
int LUA_GetScriptList(lua_State* L);
int LUA_ToHash(lua_State* L);

int luaopen_zlib(lua_State* L);
int luaopen_md5(lua_State* L);
int luaopen_base64(lua_State* L);
int luaopen_cmsgpack(lua_State* L);
int luaopen_cmsgpack_safe(lua_State* L);
int luaopen_lfs(lua_State* L);
int luaopen_uuid(lua_State* L);
int luaopen_cprint(lua_State* L);
LUAMOD_API int luaopen_ggelua(lua_State* L);